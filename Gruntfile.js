'use strict';

module.exports = function (grunt) {
  // Define the configuration for all the tasks
// Time how long tasks take. Can help when optimizing build times
require('time-grunt')(grunt);
// Automatically load required Grunt tasks
require('jit-grunt')(grunt, {
  useminPrepare: 'grunt-usemin'
});

    
    
 // Define the configuration for all the tasks configurationn here
grunt.initConfig({
  pkg: grunt.file.readJSON('package.json'),

 sass: {
            dist: {
                files: {
                    'css/styles.css': 'css/styles.scss'
                }
            }
        },
 // Make sure code styles are up to par and there are no obvious mistakes
  jshint: {
    options: {
      jshintrc: '.jshintrc'
    },
    
    all: {
      src: [
        'Gruntfile.js',
        'dist/scripts/{,*/}*.js'
      ]
    }
  },
    
  copy: {
            html: {
                files: [
                {
                    //for html
                    expand: true,
                    dot: true,
                    cwd: './',
                    src: ['*.html'],
                    dest: 'dist'
                }]                
            },
            fonts: {
                files: [
                {
                    //for font-awesome
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/font-awesome',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            }
        },

clean: {
  build: {
    src: [ 'dist/']
  }
},
    
     imagemin: {
            dynamic: {
                files: [{
                    expand: true,                  // Enable dynamic expansion
                    cwd: './',                   // Src matches are relative to this path
                    src: ['img/*.{png,jpg,gif}'],   // Actual patterns to match
                    dest: 'dist/'                  // Destination path prefix
                }]
            }
        },
    
 
        useminPrepare: {
            foo: {
                dest: 'dist',
                src: ['contactus4.html','aboutus4.html','index4.html']
            },
            options: {
                flow: {
                    steps: {
                        css: ['cssmin'],
                        js:['uglify']
                    },
                    post: {
                        css: [{
                            name: 'cssmin',
                            createConfig: function (context) {
                            var generated = context.options.generated;
                                generated.options = {
                                    keepSpecialComments: 0, rebase: false
                                };
                            }       
                        }]
                    }
                }
            }
        },

  // Concat
        concat: {
            options: {
                separator: ';'
            },
  
            // dist configuration is provided by useminPrepare
            dist: {}
        },
  
// Uglify
uglify: {
  // dist configuration is provided by useminPrepare
  dist: {}
},

cssmin: {
  dist: {}
},

 // Filerev
        filerev: {
            options: {
                encoding: 'utf8',
                algorithm: 'md5',
                length: 20
            },
  
            release: {
            // filerev:release hashes(md5) all assets (images, js and css )
            // in dist directory
                files: [{
                    src: [
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },
  
  
  release: {
    // filerev:release hashes(md5) all assets (images, js and css )
    // in dist directory
    files: [{
      src: [
        'dist/scripts/*.js',
        'dist/css/*.css',
      ]
    }]
  },

  
// Usemin
        // Replaces all assets with their revved version in html and css files.
        // options.assetDirs contains the directories for finding the assets
        // according to their relative paths
        usemin: {
            html: ['dist/contactus4.html','dist/aboutus4.html','dist/index4.html'],
            options: {
                assetsDirs: ['dist', 'dist/css','dist/js']
            }
        },
                 
    htmlmin: {                                         // Task
            dist: {                                        // Target
                options: {                                 // Target options
                    collapseWhitespace: true
                },
                files: {                                   // Dictionary of files
                    'dist/index4.html': 'dist/index4.html',  // 'destination': 'source'
                    'dist/contactus4.html': 'dist/contactus4.html',
                    'dist/aboutus4.html': 'dist/aboutus4.html',
                }
            }
        },
    
watch: {
    
   files: 'css/*.scss',
   tasks: ['sass'],    
    
  copy: {
    files: [ 'dist/**', '!dist/**/*.css', '!dist/**/*.js'],
    tasks: [ 'build' ]
  },
  
  scripts: {
    files: ['dist/scripts/scripts.js'],
    tasks:[ 'build']
  },
  
  styles: {
    files: ['dist/css/mystyles.css'],
    tasks:['build']
  },
  
  livereload: {
    options: {
      livereload: '<%= connect.options.livereload %>'
    },
    
  files: [
      'dist/{,*/}*.html',
      'css/*.scss',
      '.tmp/styles/{,*/}*.css',
      'dist/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'      
    ]
  }
},
    
 browserSync: {
            dev: {
                bsFiles: {
                    src : [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: "./"
                    }
                }
            }
        },

connect: {
  options: {
    port: 9000,
    // Change this to '0.0.0.0' to access the server from outside.
    hostname: 'localhost',
    livereload: 35729
  },
  
  dist: {
    options: {
      open: true,
      base:{
        path: 'dist',
        options: {
          index: 'index4.html',
          maxAge: 300000
        }
      }
    }
  }
},  
                 
                 
                 
                 
                 
                 
});


//register task here
    
grunt.registerTask('default', ['browserSync', 'watch']);    
    
grunt.registerTask('build', [
  'clean',
  'jshint',
  'useminPrepare',
  'concat',
  'cssmin',
  'uglify',
    'htmlmin',
  'copy',
    'imagemin',
  'filerev',
  'usemin'
]);

grunt.registerTask('serve',[
    'build','connect:dist','watch'
]);
grunt.registerTask('default',['build']);
    
};
    
