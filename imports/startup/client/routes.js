import { FlowRouter } from 'meteor/Kadira:flow-router';
import { BlazeLayout } from 'meteor/Kadira:blaze-layout';

//import need templates
import '../../ui/layouts/body/body.js';
import '../../ui/pages/home/home.js';

//set up all routes in the app
FlowRouter.route('/',{
    name:'App.home',
    action(){
       BlazeLayout.render('App_body',{main:'App_home'}); 
    }
    
    
},


);
